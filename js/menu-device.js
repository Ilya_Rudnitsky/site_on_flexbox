$(document).ready(function () {
    var obj = $(".menu-list ul");
    var body =$("body");

    if($(window).width() < '1023'){
        obj.attr("style","display: none")
    } else {
        obj.attr("style","display: flex")
    }

    $(window).resize(function () {
        if($(window).width() < '1023'){
            obj.attr("style","display: none");
            $("#line1").animate({right: '0%'});
            $("#line2").animate({right: '0%'});
            $("body").removeClass("fixed");
        } else {
            obj.attr("style","display: flex");
            $("body").removeClass("fixed");
        }
    });

    $("#menuDevice").click('on', function (event) {
        event.preventDefault();
        obj.slideToggle(1000, function () {

        });
        body.toggleClass("fixed");
        if(body.attr("class") === "") {
            $("#line1").animate({right: '+=50%'});
            $("#line2").animate({right: '+=30%'});
        } else {
            $("#line1").animate({right: '-=50%'});
            $("#line2").animate({right: '-=30%'});
        }
    });
});

